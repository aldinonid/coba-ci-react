import { useState, useRef, useEffect } from 'react'
import VideoPlayer from "./Components/VideoPlayer";

import axios from 'axios'

const App = () => {
  const [data, setData] = useState({
    audio_list: {},
    thumbnails: [],
    video_list: []
  })
  const [playVideo, setPlayVideo] = useState("")
  const [postVideo, setPostVideo] = useState("")
  const [isMobile, setIsMobile] = useState(false)
  const [isChrome, setIsChrome] = useState(true)
  // const [thumbnail, setThumbnail] = useState("")  
  const player = useRef(null)

  useEffect(() => {
    detectDeviceType()
    detectBrowserType()
    getData()
  }, [])

  const getData = () => {
    axios({
      method: 'get',
      url: process.env.REACT_APP_API_URL,
    })
      .then(res => {
        const videoList = res.data.video_list
        // const thumbnail = res.data.thumbnails
        setPlayVideo(videoList[videoList.length - (videoList.length - 1)].url)
        // setThumbnail(thumbnail[thumbnail.length - 1].url)
        setData(res.data)
      });
  }

  const handleChangeResolution = videoUrl => {
    setPlayVideo(videoUrl)
    player.current.videoPlayer.current.handleReady()
    player.current.audioPlayer.current.seekTo(parseFloat(0))
  }

  const handleExportVideo = () => {
    axios({
      method: 'get',
      url: process.env.REACT_APP_API_URL,
      params: {
        url: `${postVideo}`
      }
    })
    .then(res => {
      const videoList = res.data.video_list
      setPlayVideo(videoList[videoList.length - (videoList.length - 1)].url)
      setData(res.data)
      setPostVideo("")
    })
  }

  const detectDeviceType = () => setIsMobile(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent));

  const detectBrowserType = () => setIsChrome(/Chrome/i.test(navigator.userAgent))

  if (isMobile) {
    return (
      <>
        <h1>Please change your device to see the video</h1>
        <h1>Because this player is not compatible with mobile</h1>
      </>
    )
  } else if (!isChrome) {
    return (
      <>
        <h1>Please change your browser to see the video</h1>
        <h1>Because this player is not compatible other than Google Chrome browser</h1>
        <h1>Because this RELEASE BRANCH</h1>
      </>
    )
  } else {
    return (
      <>
        <input type="text" value={postVideo} onChange={e => setPostVideo(e.target.value)} />
        <button onClick={handleExportVideo} >Export Video</button>
    
        <br />
        {data.video_list?.map((video, index) => (
          <button
            key={index} 
            onClick={() => handleChangeResolution(video.url)}
          >
            {video.qualityLabel}
          </button>
        ))}
    
        <VideoPlayer 
          ref={player}
          videoUrl={playVideo} 
          audioUrl={data.audio_list?.url}
        />
      </>
    )
  }
}

export default App;

