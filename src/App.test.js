import { render, screen } from '@testing-library/react';
import VideoConvert from './Pages/VideoConvert';

test('have a button element to able click', () => {
  render(<VideoConvert />);
  const linkElement = screen.getByRole('button', {name: "Create Video!"})
  expect(linkElement).toBeInTheDocument();
});
