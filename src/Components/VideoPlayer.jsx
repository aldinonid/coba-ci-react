import { useEffect, useRef, useState, useImperativeHandle, forwardRef } from 'react'
import ReactPlayer from 'react-player'
import ReactLoading from 'react-loading'
import './VideoStyle.css'

const VideoPlayer = forwardRef((props, ref) => {
  const [state, setState] = useState({
    playingVideo: false,
    playingAudio: false,
    played: 0,
    loading: false,
    volume: 1,
    muted: false,
    pip: true,
    duration: 0,
  });
  const [rememberVolume, setRememberVolume] = useState(0)
  const playerVideo = useRef(null);
  const playerAudio = useRef(null);
  const container = useRef(null)

  useEffect(() => {
    window.addEventListener('visibilitychange', () => {
      if (document.hidden) {
        if (state.playingAudio && state.playingVideo) {
          setState({ 
            ...state, 
            playingAudio: false, 
            playingVideo: false 
          })
        }
      } else {
        setState({
          ...state, 
          playingAudio: true, 
          playingVideo: true 
        })
      }
    })
  }, [state])

  useImperativeHandle(ref, () => ({
    state,
    videoPlayer: playerVideo,
    audioPlayer: playerAudio
  }), [state, playerVideo, playerAudio])

  const handleReady = () => setState({ ...state, loading: false });
  
  const handleProgress = (stateProgress) => {
    if (!state.seeking) {
      setState({ ...state, played: stateProgress.played });
    }
  };

  const handleSeekMouseDown = () =>
    setState({ ...state, seeking: true, playingAudio: false, playingVideo: false  });

  const handleSeekChange = (e) =>
    setState({ ...state, played: parseFloat(e.target.value) });

  const handleSeekMouseUp = (e) => {
    setState({
      ...state,
      seeking: false,
      loading: true,
      playingAudio: false, 
      playingVideo: false 
    });
    playerVideo.current.seekTo(parseFloat(e.target.value));
    playerAudio.current.seekTo(parseFloat(e.target.value));
  };

  const handlePlayPause = () =>
    setState({ ...state, playingAudio: !state.playingAudio, playingVideo: !state.playingVideo });
  
  const handleToggleMute = () => {
    if (state.muted) {
      setState({ ...state, muted: !state.muted, volume: rememberVolume })
    } else {
      setRememberVolume(state.volume)
      setState({ ...state, muted: !state.muted, volume: 0 })
    }
  }

  const handleVolumeChange = e => setState({ ...state, volume: parseFloat(e.target.value), muted: false });

  const handleTogglePIP = () => setState({ ...state, pip: !state.pip })

  const handleFullscreen = () => {
    const el = document.getElementById("videoPlayer");
    if (el.requestFullscreen) {
      el.requestFullscreen();
    }
  }

  const handleBufferStart = () => setState({ ...state, playingAudio: false, playingVideo: false });

  const handleBufferEnd = () => setState({ ...state, playingAudio: true, playingVideo: true });

  const handleDuration = duration => setState({ ...state, duration: duration });

  const handleOnStartVideo = () => setState({ ...state, playingAudio: true })

  const handleOnPauseVideo = () => setState({ ...state, playingAudio: false })

  return (
    <div ref={container} className="container show-controls">
      {state.loading ? (
        <div style={{ 
          position: 'absolute',
          height: "100%", 
          width: "100%",
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center'
        }}>
          <ReactLoading type='spin' />
        </div>
      ) : (
        <div className="wrapper">
          <div className="video-timeline">
            <input 
              type="range"
              min={0}
              max={0.999999}
              step="any"
              value={state.played}
              onMouseDown={handleSeekMouseDown}
              onChange={handleSeekChange}
              onMouseUp={handleSeekMouseUp}
            />
          </div>
          <ul className="video-controls">
            <li className="options left">
              <button className='volume' onClick={handleToggleMute}>
                {state.muted && state.volume === 0 ? (
                  <svg 
                    fill="white"
                    xmlns="http://www.w3.org/2000/svg" 
                    width="23" 
                    height="23" 
                    viewBox="0 0 23 23"
                  >
                    <path d="M19 7.358v15.642l-8-5v-.785l8-9.857zm3-6.094l-1.548-1.264-3.446 4.247-6.006 3.753v3.646l-2 2.464v-6.11h-4v10h.843l-3.843 4.736 1.548 1.264 18.452-22.736z"/>
                  </svg>
                ) : (
                  <svg 
                    fill="white"
                    xmlns="http://www.w3.org/2000/svg" 
                    width="23" 
                    height="23" 
                    viewBox="0 0 23 23"
                  >
                    <path d="M6 7l8-5v20l-8-5v-10zm-6 10h4v-10h-4v10zm20.264-13.264l-1.497 1.497c1.847 1.783 2.983 4.157 2.983 6.767 0 2.61-1.135 4.984-2.983 6.766l1.498 1.498c2.305-2.153 3.735-5.055 3.735-8.264s-1.43-6.11-3.736-8.264zm-.489 8.264c0-2.084-.915-3.967-2.384-5.391l-1.503 1.503c1.011 1.049 1.637 2.401 1.637 3.888 0 1.488-.623 2.841-1.634 3.891l1.503 1.503c1.468-1.424 2.381-3.309 2.381-5.394z"/>
                  </svg>
                )}
              </button>
              <input 
                type="range" 
                value={state.volume}
                onChange={handleVolumeChange}
                min={0}
                max={1}
                step="any"
              />
              <div className="video-timer">
                <Duration className="current-time" seconds={state.duration * state.played} />
                <p className="separator"> / </p>
                <Duration className="video-duration" seconds={state.duration} />
              </div>
            </li>
            <li className="options center">
              <button onClick={handlePlayPause} className="play-pause">
                {state.playingVideo && state.playingAudio ? (
                  <svg
                    fill="white"
                    xmlns="http://www.w3.org/2000/svg"
                    width="23"
                    height="23"
                    viewBox="0 0 24 24"
                  >
                    <path d="M10 24h-6v-24h6v24zm10-24h-6v24h6v-24z" />
                  </svg>
                ) : (
                  <svg
                    fill="white"
                    xmlns="http://www.w3.org/2000/svg"
                    width="23"
                    height="23"
                    viewBox="0 0 24 24"
                  >
                    <path d="M2 24v-24l20 12-20 12z" />
                  </svg>
                )}
              </button>
            </li>
            <li className="options right">
              <button className='pic-in-pic' onClick={handleTogglePIP}>
                <svg fill="white" xmlns="http://www.w3.org/2000/svg" width="23" height="23" viewBox="0 0 24 24">
                  <path d="M0 3v18h24v-18h-24zm22 16h-4v-10h-16v-4h20v14z"/>
                </svg>
              </button>
              <button className='fullscreen' onClick={handleFullscreen}>
                <svg fill='white' xmlns="http://www.w3.org/2000/svg" width="23" height="23" viewBox="0 0 24 24">
                  <path d="M24 9h-2v-5h-7v-2h9v7zm-9 13v-2h7v-5h2v7h-9zm-15-7h2v5h7v2h-9v-7zm9-13v2h-7v5h-2v-7h9z"/>
                </svg>
              </button>
            </li>
          </ul>
        </div>
      )}
      <ReactPlayer
        url={props.videoUrl}
        ref={playerVideo}
        onProgress={handleProgress}
        playing={state.playingVideo}
        onReady={handleReady}
        pip={state.pip}
        width="100%"
        height="100%"
        onDuration={handleDuration}
        onBuffer={handleBufferStart}
        onBufferEnd={handleBufferEnd}
        controls={false}
        loop={false}
        config={{ file: { 
          attributes: {
            id: "videoPlayer",
            controlsList: 'nodownload',
            onContextMenu: e => e.preventDefault()
          }
        }}}
        onStart={handleOnStartVideo}
        onPause={handleOnPauseVideo}
        // light={props.thumbnail}
      />
      <ReactPlayer
        url={props.audioUrl}
        ref={playerAudio}
        onProgress={handleProgress}
        volume={state.volume}
        muted={state.muted}
        playing={state.playingAudio}
        loop={false}
        onBuffer={handleBufferStart}
        onBufferEnd={handleBufferEnd}
        // light={props.thumbnail}
        style={{ position: "absolute", visibility: "hidden" }}
      />
    </div>
  )
})

const Duration = ({ className, seconds }) => {

  const pad = string => ('0' + string).slice(-2)

  const format = () => {
    const date = new Date(seconds * 1000)
    let hh = date.getUTCHours()
    let mm = date.getUTCMinutes()
    const ss = pad(date.getUTCSeconds())
    if (hh) {
      return `${hh}:${pad(mm)}:${ss}`
    }
    return `${pad(mm)}:${ss}`
  }
  
  return <time className={className} dateTime={`P${Math.round(seconds)}S`}>
    {format(seconds)}
  </time>
}

export default VideoPlayer


