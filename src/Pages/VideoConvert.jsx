import { useState } from "react";
import { createFFmpeg, fetchFile } from '@ffmpeg/ffmpeg'

const VideoConvert = () => {
  const [videoSrc, setVideoSrc] = useState("")
  const [videoFile, setVideoFile] = useState({})
  const [audioFile, setAudioFile] = useState({})

  const handleVideoFile = e => {
    const videoFile = e.target.files[0]
    setVideoFile(videoFile)
    console.log(videoFile)
  }

  const handleAudioFile = e => {
    const audioFile = e.target.files[0]
    setAudioFile(audioFile)
    console.log(audioFile)
  }

  const ffmpeg = createFFmpeg({
    log: true,
  })

  const createVideo = async () => {
    await ffmpeg.load();
    ffmpeg.FS("writeFile", "video.mp4", await fetchFile(videoFile))
    ffmpeg.FS("writeFile", "sound.mp3", await fetchFile(audioFile))
    await ffmpeg.run("-i", "video.mp4", "-i", "sound.mp3", "-c:v", "copy", "-c:a", "aac", "test.mp4");
    const data = ffmpeg.FS("readFile", "test.mp4");
    console.log("testing data", data)
    setVideoSrc(URL.createObjectURL(new Blob([data.buffer], {type: "video/mp4"})))
  }

  return (
    <div>
      <video src={videoSrc} controls />
      <input type="file" accept="video/*" onChange={handleVideoFile} />
      <input type="file" accept="audio/*" onChange={handleAudioFile} />
      <hr />
      <button onClick={createVideo}>Create Video!</button>
    </div>
  );
}

export default VideoConvert