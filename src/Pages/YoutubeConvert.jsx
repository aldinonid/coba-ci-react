import { useState, useRef } from "react";
import ReactPlayer from "react-player";
import ReactLoading from "react-loading";
import "./YoutubeConvert.css";

const linkVideo =
  "https://rr3---sn-npoe7nsl.googlevideo.com/videoplayback?expire=1663569581&ei=TbonY4mzMsmWwgOHi7CYDg&ip=13.250.154.11&id=o-AE4Q9-i8aML8p9a9oZlotT0IW_96kf0nwBhXZO4_L0ZN&itag=137&source=youtube&requiressl=yes&mh=RL&mm=31%2C29&mn=sn-npoe7nsl%2Csn-npoeens7&ms=au%2Crdu&mv=m&mvi=3&pl=17&initcwndbps=1117500&vprv=1&svpuc=1&mime=video%2Fmp4&gir=yes&clen=57881744&dur=269.933&lmt=1597302896792754&mt=1663547583&fvip=3&keepalive=yes&fexp=24001373%2C24007246&beids=24277534&c=ANDROID&rbqsm=fr&txp=5432432&sparams=expire%2Cei%2Cip%2Cid%2Citag%2Csource%2Crequiressl%2Cvprv%2Csvpuc%2Cmime%2Cgir%2Cclen%2Cdur%2Clmt&sig=AOq0QJ8wRQIhAPsQMp2yDZoBtceF_EhqEX6fYpGncMyByv1UsdfXmtQ_AiBZIiIZsyZfKiJpJMDx262IAfzVk9nvd8XNr171oyfnmQ%3D%3D&lsparams=mh%2Cmm%2Cmn%2Cms%2Cmv%2Cmvi%2Cpl%2Cinitcwndbps&lsig=AG3C_xAwRQIgPJLvbT3ITkxAL6-hfk3ME0oahvYRsKNKAQNuRnhFbccCIQDH6e2DfTo84p_CX7enW05_1q-LdxusTSfe6rM3QwmVXg%3D%3D";
const linkAudio =
  "https://rr3---sn-npoe7nsl.googlevideo.com/videoplayback?expire=1663569581&ei=TbonY4mzMsmWwgOHi7CYDg&ip=13.250.154.11&id=o-AE4Q9-i8aML8p9a9oZlotT0IW_96kf0nwBhXZO4_L0ZN&itag=251&source=youtube&requiressl=yes&mh=RL&mm=31%2C29&mn=sn-npoe7nsl%2Csn-npoeens7&ms=au%2Crdu&mv=m&mvi=3&pl=17&initcwndbps=1117500&vprv=1&svpuc=1&mime=audio%2Fwebm&gir=yes&clen=4307949&dur=269.961&lmt=1597302687527086&mt=1663547583&fvip=3&keepalive=yes&fexp=24001373%2C24007246&beids=24277534&c=ANDROID&rbqsm=fr&txp=5431432&sparams=expire%2Cei%2Cip%2Cid%2Citag%2Csource%2Crequiressl%2Cvprv%2Csvpuc%2Cmime%2Cgir%2Cclen%2Cdur%2Clmt&sig=AOq0QJ8wRQIgKq0U8gc1_Ox9L8YD_XE_d7b4hjpjFyxpgwPv9fQQgnwCIQCiPkZqL_kf_4xqeQSEf_aNE_raYqA788wFOiLLzx6COA%3D%3D&lsparams=mh%2Cmm%2Cmn%2Cms%2Cmv%2Cmvi%2Cpl%2Cinitcwndbps&lsig=AG3C_xAwRQIgPJLvbT3ITkxAL6-hfk3ME0oahvYRsKNKAQNuRnhFbccCIQDH6e2DfTo84p_CX7enW05_1q-LdxusTSfe6rM3QwmVXg%3D%3D";

const YoutubeConvert = () => {
  const [state, setState] = useState({
    playing: false,
    played: 0,
    loading: true,
    ready: false
  });
  const player1 = useRef(null);
  const player2 = useRef(null);

  const handleSeekMouseDown = () =>
    setState({ ...state, seeking: true, playing: false, loading: true });

  const handleSeekChange = (e) =>
    setState({ ...state, played: parseFloat(e.target.value) });

  const handleSeekMouseUp = (e) => {
    setState({
      ...state,
      seeking: false,
      playing: false,
      loading: true,
      ready: false
    });
    player1.current.seekTo(parseFloat(e.target.value));
    player2.current.seekTo(parseFloat(e.target.value));
  };

  const handleProgress = (stateProgress) => {
    if (!state.seeking) {
      setState({ ...state, played: stateProgress.played });
    }
  };

  const handleReady = () => setState({ ...state, ready: true });

  const handlePause = () =>
    setState({ ...state, playing: !state.playing, loading: !state.loading });

  return (
    <div>
      <div className="videoContainer">
        {!state.ready && (
          <div className="spinnerWrapper">
            <ReactLoading type="spin" className="test" />
          </div>
        )}
        <ReactPlayer
          url={linkVideo}
          ref={player1}
          onProgress={handleProgress}
          playing={state.playing}
          onReady={handleReady}
          width="100%"
          height="100%"
        />
      </div>
      <div style={{ position: "absolute", visibility: "hidden" }}>
        <ReactPlayer
          url={linkAudio}
          ref={player2}
          onProgress={handleProgress}
          playing={state.playing}
        />
      </div>
      {state.ready && (
        <>
          <button onClick={handlePause}>
            {state.playing ? "Pause" : "Play"}
          </button>
          <input
            type="range"
            min={0}
            max={0.999999}
            step="any"
            value={state.played}
            onMouseDown={handleSeekMouseDown}
            onChange={handleSeekChange}
            onMouseUp={handleSeekMouseUp}
          />
          <h1>Test Hidden</h1>
        </>
      )}
    </div>
  );
};

export default YoutubeConvert;
